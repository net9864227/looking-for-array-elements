﻿using System;

#pragma warning disable

namespace LookingForArrayElements
{
    public static class DecimalCounter
    {
        /// <summary>
        /// Searches an array of decimals for elements that are in a specified range, and returns the number of occurrences of the elements that matches the range criteria.
        /// </summary>
        /// <param name="arrayToSearch">One-dimensional, zero-based <see cref="Array"/> of single-precision floating-point numbers.</param>
        /// <param name="ranges">One-dimensional, zero-based <see cref="Array"/> of range arrays.</param>
        /// <returns>The number of occurrences of the <see cref="Array"/> elements that match the range criteria.</returns>
        public static int GetDecimalsCount(decimal[] arrayToSearch, decimal[][] ranges)
        {
            int i = 0;
            int counter = 0;
            if (arrayToSearch is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch));
            }
            if (ranges is null)
            {
                throw new ArgumentNullException(nameof(ranges));
            }

            if (ranges.Length == 0) return 0;
            do
            {
                if (ranges[i] is null) throw new ArgumentNullException(nameof(ranges));
                if (ranges[i].Length > 2 || ranges[i].Length == 1) throw new ArgumentException(string.Empty, nameof(ranges));
                if (ranges[i].Length != 0 && ranges[i][0] > ranges[i][1]) throw new ArgumentException(string.Empty, nameof(ranges));
                i++;
            }
            while (i < ranges.Length);

            if (arrayToSearch.Length == 0) return 0;
            i = 0;
            int j = 0;
            do
            {
                j = 0;

                do
                {
                    if (ranges[j].Length!=0)
                    {
                        if (arrayToSearch[i] >= ranges[j][0] && arrayToSearch[i] <= ranges[j][1])
                        {
                            counter++;
                            break;
                        }
                    }
                    j++;
                }
                while (j < ranges.Length);
                i++;
            } 
            while (i < arrayToSearch.Length);

            return counter;

        }

        /// <summary>
        /// Searches an array of decimals for elements that are in a specified range, and returns the number of occurrences of the elements that matches the range criteria.
        /// </summary>
        /// <param name="arrayToSearch">One-dimensional, zero-based <see cref="Array"/> of single-precision floating-point numbers.</param>
        /// <param name="ranges">One-dimensional, zero-based <see cref="Array"/> of range arrays.</param>
        /// <param name="startIndex">The zero-based starting index of the search.</param>
        /// <param name="count">The number of elements in the section to search.</param>
        /// <returns>The number of occurrences of the <see cref="Array"/> elements that match the range criteria.</returns>
        public static int GetDecimalsCount(decimal[] arrayToSearch, decimal[][] ranges, int startIndex, int count)
        {
            if (arrayToSearch is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch));
            }

            if (ranges is null)
            {
                throw new ArgumentNullException(nameof(ranges));
            }

            for(int i = 0; i < ranges.Length;i++)
            {
                if (ranges[i] is null) throw new ArgumentNullException(nameof(ranges));
                if (ranges[i].Length!=2 && ranges[i].Length != 0) throw new ArgumentException(string.Empty, nameof(ranges));          
                if(ranges[i].Length!=0 && ranges[i][0] > ranges[i][1]) throw new ArgumentException(string.Empty, nameof(ranges));
            }

            if (startIndex < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), "startIndex is less than zero");
            }

            if (startIndex > arrayToSearch.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), "startIndex is greater than arrayToSearch.Length");
            }

            if (count < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(count), "count is less than zero");
            }

            int lastPosition = startIndex + count;
            if (lastPosition > arrayToSearch.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(count), "startIndex + count > arrayToSearch.Length");
            }

            if (arrayToSearch.Length == 0 || ranges.Length == 0) return 0;

            int counter = 0;

            for (int i = startIndex; i < lastPosition; i++)
            {
                for (int j = 0; j < ranges.Length; j++)
                {
                        if (arrayToSearch[i] >= ranges[j][0] && arrayToSearch[i] <= ranges[j][1])
                        {
                            counter++;
                            break;
                        }
                }

            }
            return counter;
        }
    }
}
