﻿using System;

#pragma warning disable

namespace LookingForArrayElements
{
    public static class FloatCounter
    {
        /// <summary>
        /// Searches an array of floats for elements that are in a specified range, and returns the number of occurrences of the elements that matches the range criteria.
        /// </summary>
        /// <param name="arrayToSearch">One-dimensional, zero-based <see cref="Array"/> of single-precision floating-point numbers.</param>
        /// <param name="rangeStart">One-dimensional, zero-based <see cref="Array"/> of the range starts.</param>
        /// <param name="rangeEnd">One-dimensional, zero-based <see cref="Array"/> of the range ends.</param>
        /// <returns>The number of occurrences of the <see cref="Array"/> elements that match the range criteria.</returns>
        public static int GetFloatsCount(float[] arrayToSearch, float[] rangeStart, float[] rangeEnd)
        {
            if (arrayToSearch == null) throw new ArgumentNullException(nameof(arrayToSearch));
            if (rangeStart == null) throw new ArgumentNullException(nameof(rangeStart));
            if (rangeEnd == null) throw new ArgumentNullException(nameof(rangeEnd));

            if (rangeStart.Length != rangeEnd.Length)
            {
                throw new ArgumentException(string.Empty, nameof(rangeStart));
            }

            int a = 0;
            do
            {
                if (a == rangeEnd.Length) break;

                if (rangeStart[a] > rangeEnd[a])
                {
                    throw new ArgumentException(string.Empty, nameof(rangeEnd));
                }
                a++;
            }
            while (a < rangeEnd.Length);

            int counter = 0;
            for (int i = 0; i < arrayToSearch.Length; i++)
            {
                for (int j = 0; j < rangeStart.Length; j++)
                {
                    if (arrayToSearch[i] >= rangeStart[j] && arrayToSearch[i] <= rangeEnd[j]) counter++;
                }
            }

            return counter;
        }

        /// <summary>
        /// Searches an array of floats for elements that are in a specified range, and returns the number of occurrences of the elements that matches the range criteria.
        /// </summary>
        /// <param name="arrayToSearch">One-dimensional, zero-based <see cref="Array"/> of single-precision floating-point numbers.</param>
        /// <param name="rangeStart">One-dimensional, zero-based <see cref="Array"/> of the range starts.</param>
        /// <param name="rangeEnd">One-dimensional, zero-based <see cref="Array"/> of the range ends.</param>
        /// <param name="startIndex">The zero-based starting index of the search.</param>
        /// <param name="count">The number of elements in the section to search.</param>
        /// <returns>The number of occurrences of the <see cref="Array"/> elements that match the range criteria.</returns>
        public static int GetFloatsCount(float[] arrayToSearch, float[] rangeStart, float[] rangeEnd, int startIndex, int count)
        {
            if (arrayToSearch is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch));
            }

            if (rangeStart == null)
            {
                throw new ArgumentNullException(nameof(rangeStart));
            }
            if (rangeEnd == null)
            {
                throw new ArgumentNullException(nameof(rangeEnd));
            }

            if (rangeStart.Length!=rangeEnd.Length)
            {
                throw new ArgumentException(string.Empty, nameof(rangeStart));
            }

            if (startIndex < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), "startIndex is less than zero");
            }

            if (startIndex > arrayToSearch.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), "startIndex is greater than arrayToSearch.Length");
            }

            if (count < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(count), "count is less than zero");
            }

            int i = 0;
            do
            {
                if (i == rangeEnd.Length) break;

                if (rangeStart[i] > rangeEnd[i])
                {
                    throw new ArgumentException(string.Empty, nameof(rangeEnd));
                }
                i++;
            }
            while (i < rangeEnd.Length);

            if (count == 0 || rangeEnd.Length == 0 || rangeStart.Length == 0) return 0;

            int lastPosition = startIndex + count;
            if (lastPosition > arrayToSearch.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(count), "startIndex + count > arrayToSearch.Length");
            }

            int counter = 0;
            do
            {
                i = 0;
                do
                {
                    if (arrayToSearch[startIndex] >= rangeStart[i] && arrayToSearch[startIndex] <= rangeEnd[i])
                    {
                        counter++;
                    }

                    i++;
                }
                while (i < rangeEnd.Length);

                startIndex++;
            }
            while (startIndex < lastPosition);
            return counter;

        }
    }
}
